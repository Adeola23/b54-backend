import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from './product/product.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { CartModule } from './cart/cart.module';
import { TwilioModule } from 'nestjs-twilio';
import { MailerModule } from './mailer/mailer.module';
import { TwiliModule } from './twilio/twilio.module';

@Module({
  imports: [
   
    MongooseModule.forRoot(process.env.MONGO1_URI),
    ProductModule,
    AuthModule,
    UserModule,
    CartModule,
    MailerModule,
    TwiliModule,
  ],
})
export class AppModule {}
