import { Injectable } from '@nestjs/common';
import { InjectTwilio, TwilioClient } from 'nestjs-twilio';
import { UserService } from '../user/user.service';

@Injectable()
export class TwilioService {
  constructor(
    @InjectTwilio() private readonly client: TwilioClient,
    private readonly userService: UserService,
  ) {}
  async sendSMS(Id: string) {
    const user = await this.userService.findUserById(Id);
    if (user) {
      try {
        var name = [];
        for (let cart of user.shoppingCart) {
          if (name.length % 2 == 0) {
            name.push(cart.product_description);
          } else {
            name.push('and' + '  ' + cart.product_description);
          }
        }
        console.log('fish');
        return await this.client.messages.create({
          body: `SMS Body, sent to the phone! ${name}`,
          from: process.env.TWILIO_PHONE_NUMBER,
          to: '+2348131902811',
        });
      } catch (e) {
        return e;
      }
    }
  }
}
