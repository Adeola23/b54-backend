import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import {User, UserSchema } from './schema/user.schema';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { CartModule} from 'src/cart/cart.module';
import { CartService } from 'src/cart/cart.service';
import { ProductService } from 'src/product/product.service';
import { ProductModule } from 'src/product/product.module';
import { ProductSchema, Product } from 'src/product/schema/product.schema';
import {MailerService} from 'src/mailer/mailer.service'
import { TwilioService } from 'src/twilio/twilio.service';
import { TwilioModule } from 'nestjs-twilio';
import { TwiliModule } from 'src/twilio/twilio.module';


@Module({
  imports:[
    MongooseModule.forFeature([{name: User.name, schema:UserSchema}]),
    PassportModule.register({defaultStrategy:'jwt'}), JwtModule.register({
    secret: 'topSecret51',
    signOptions:{
      expiresIn: 3600
    }
    }), 
    TwilioModule.forRoot({
      accountSid: process.env.TWILIO_ACCOUNT_SID,
      authToken: process.env.TWILIO_AUTH_TOKEN,
    }),
    MongooseModule.forFeature([{name: Product.name, schema:ProductSchema}]),
    CartModule,
    TwiliModule
    

  ],
  controllers: [UserController],
  providers: [UserService, JwtStrategy, CartService, ProductService, MailerService, TwilioService],
  exports: [JwtStrategy, PassportModule]
})
export class UserModule {}
