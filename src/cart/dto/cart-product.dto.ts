import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  MaxLength,
  Matches,
} from 'class-validator';

export class CartItemDto {

    @IsString()
    productId: string;

    @IsString()
    userId: string;

}