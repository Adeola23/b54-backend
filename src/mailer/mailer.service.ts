import * as nodemailer from 'nodemailer';
import { Inject, Injectable } from '@nestjs/common';
import aws from 'aws-sdk';

import { UserService } from '../user/user.service';

@Injectable()
export class MailerService {
  constructor(private readonly userService: UserService) {}

  async SendMail(Id: string) {
    const user = await this.userService.findUserById(Id);
    if (user) {
      var name = [];
      for (let cart of user.shoppingCart) {
        name.push(cart.product_description);
      }

      function and(names, conjunction = 'and') {
        const named = names.slice(0, names.length - 1)
        if (names) {
          return [named] + ' ' + 'and' + ' ' + names[names.length - 1];
        }
      }

      const mailerClient: nodemailer.Transporter = nodemailer.createTransport({
        host: 'email-smtp.us-east-1.amazonaws.com',
        port: 587,
        auth: {
          user: 'AKIA6FBREOUWYOO3WJ7Q',
          pass: 'BLstpEjrvnCxRjvbgdMVWAIsVi/WTl7i3+hUFxXCu36E',
        },
      });

      console.log('fish');

      return mailerClient.sendMail({
        from: 'Adeola amosadeola92@gmail.com',
        to: 'amosadeola92@gmail.com',
        subject: 'Thank you for your order ',
        html: `Thank you for ordering ${and(name)}`,
      });
    }
  }
}
