FROM  node:14.17.6

WORKDIR /app

COPY *.json /app/

RUN npm install

COPY . /app/

CMD ["npm", "start"]